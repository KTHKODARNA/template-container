#include <iostream>
#include <stdexcept>
#include <type_traits>

template <typename T> class Vector {
public:

	typedef T value_type;

	static_assert(std::is_move_constructible<value_type>::value, "The specified element type must be MoveConstructible");
	static_assert(std::is_move_assignable<value_type>::value, "The specified element type must be MoveAssignable");
	T* v;

	int length;
	int cap;




	/**
	 * Default constructor
	 */
	 Vector() {
	 	v = new T[0];
	 	length = 0;
	 	cap = 0;
	 	std::clog << "default-constructor" << std::endl;
	 	

	 }

	/**
	 * size_t constructor
	 */
	 explicit Vector(const std::size_t &n) {
	 	std::clog << "size_t-constructor" << std::endl;
	 	v = new T[n];
	 	length = n;
	 	cap = n;
	 }

	 /**
	  * Init with value-constructor
	  */
	  Vector(const std::size_t &n, const T &value) {
	  	std::clog << "init with value-constructor" << std::endl;
	  	v = new T[n];
	  	length = n;
	  	cap = n;
	  	for(int i=0; i<n; i++)
	  		v[i] = value;
	  }

	/**
	 * Destructor
	 */
	 ~Vector() {
	 	std::clog << "destructor" << std::endl;
	 	if(v != NULL) {
	 		delete [] v;
	 	}
	 }

	/**
	 * Initializer_list constructor
	 */
	 Vector(std::initializer_list<T> args) {
	 	length = args.size();
	 	cap = length;
	 	std::clog << "initializer_list-constructor" << std::endl;
	 	v = new T[length];
		typename std::initializer_list<T>::iterator it;  // same as: const int* it
		int i = 0;
		for (it=args.begin(); it != args.end(); ++it) {
			v[i] = *it;
			i++;
		}
	}

	/**
	 * Move constructor
	 */
	 Vector(Vector &&vec) {
	 	std::clog << "move-constructor" << std::endl;
	 	length = vec.length;
	 	cap = vec.cap;
	 	v = vec.v;

	 	vec.length = 0;
		//TODO: hantera gamla pekaren.
	 	vec.reset();
	 	vec.v = NULL;
	 }

	 /**
	  * Copy constructor
	  */
	  Vector(const Vector &vec) {
	  	std::clog << "copy-constructor" << std::endl;
	  	length = vec.length;
	  	cap = vec.cap;

	  	v = new T[cap]; // kanske cap.
	  	for(int i=0; i<length; i++) {
	  		v[i] = vec[i];
	  	}
	  }

	/**
	 * Subscript operator []
	 */
	 T& operator[] (const int n) {

	 	if(n < 0 || n >= length) {
	 		throw std::out_of_range("Index out of bounds");
	 	}
	 	return v[n];
	 }

	 const T& operator[] (const int n) const {
	 	if(n < 0 || n >= length) {
	 		throw std::out_of_range("Index out of bounds");
	 	}
	 	return v[n];
	 }

	/**
	 * Copy assignment operator
	 */
	 Vector& operator= (const Vector& vec) {
	 	if(v == vec.v) {
	 		return *this;
	 	}
	 	std::clog << "copy-assignment" << std::endl;
	 	length = vec.length;
	 	cap = vec.cap;
	 	if(v != NULL)
	 		delete [] v;
	 	v = new T[length]; //kanske cap.

	 	for(int i=0; i<length; i++) {
	 		v[i] = vec[i];
	 	}

	 	return *this;
	 }

	/**
	 * Move assignment operator
	 */
	 Vector& operator= (Vector&& vec) {
	 	if(v == vec.v) {
	 		return *this;
	 	}
	 	std::clog << "move-assignment" << std::endl;
	 	length = vec.length;
	 	cap = vec.cap;
	 	if(v != NULL)
	 		delete [] v;
	 	v = vec.v;

	 	vec.length = 0;
	 	vec.reset();
	 	vec.v = NULL;

	 	return *this;
	 }


	 void reserve() {

	 }

	 int reAllocate() {
	 	return length*2;
	 }

	 void push_back(const T& value) {
	 	if(length == 0) {
	 		std::clog << "Vi hade bara: v = new T[0];" << std::endl;
	 		delete[] v; // kan bli minnes-grej
	 		v = new T[1];
	 		length = 1;
	 		cap = 1;
	 		v[0] = value;
	 		return;
	 	}

	 	if(size() == cap) {
	 		std::clog << "REALLOCATING" << std::endl;
	 		int m = reAllocate();
	 		cap = m;
	 		T* temp = new T[m];

	 		for(int i=0; i<length; i++) {
	 			temp[i] = v[i];
	 		}

	 		delete [] v;
	 		v = new T[m];

	 		for(int i=0; i<length; i++) {
	 			v[i] = temp[i];
	 		}

	 		delete [] temp;
	 		v[length] = value;
	 		length++;
	 		return;

	 	} else {
	 		v[length] = value;
	 		length++;
	 		return;
	 	}
	 }
	 
	 //TODO: fixa så cap uppdateras ordentligt
	 // expanderar från 2 till 3. Detta är tydligen
	 // för lite för kattis.
	 void insert(const std::size_t &n, const T &value) {
	 	if(n == size()) {
	 		push_back(value);
	 		return;
	 	}

	 	if(size() == cap) {
	 		cap = reAllocate();
	 	}

	 	if(n == 0) {
	 		T* tmp = new T[size()];

	 		for(int i = 0; i < size(); i++) {
	 			tmp[i] = v[i];
	 		}

	 		delete [] v;
	 		v = new T[cap];
	 		v[0] = value;
	 		length++;

	 		for(int i = 1; i < size(); i++) {
	 			v[i] = tmp[i-1];
	 		}
	 		delete [] tmp;
	 		return;
	 	} 

	 	T* tmp = new T[size()];
	 	for(int i = 0; i < size(); i++)
	 		tmp[i] = v[i];

	 	delete [] v;


	 	v = new T[cap];
	 	v[n] = value;
	 	length++;
	 	for(int i = 0; i < n; i++) {
	 		v[i] = tmp[i];
	 	}

	 	for(int i = n+1; i < size(); i++) {
	 		v[i] = tmp[i-1];
	 	}
	 	delete [] tmp;
	 }

	 void erase(const std::size_t &n) {
	 	if(n < 0 || n >= size()) {
	 		throw std::out_of_range("Index out of bounds");
	 	}
	 	
	 	length--;
	 	for(int i = n; i < size(); i++)
	 		v[i] = v[i+1];
	 }

	 void clear() {
	 	reset();
	 	length = 0;
	 }


	 void reset() {
	 	for(int i = 0; i < length; i++)
	 		v[i] = {};
	 }

	 std::size_t size() const {
	 	return length;
	 }

	 int capacity() const {
	 	return cap;
	 }

	 T* begin() const {
	 	return v;
	 }


	 T* end() const{
	 	T* p = v;
	 	return p + size();
	 }

	 T* find(T const& value) const {
	 	T* p = v;
	 	for(int i = 0; i < size(); i++) {
	 		if(v[i] == value) {
	 			break;
	 		} else {
	 			p++;
	 		}
	 	}
	 	return p;
	 }




	};